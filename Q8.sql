SELECT
	q.item_id,
	q.item_name,
	q.item_price,
	qc.category_name
FROM
    item q
INNER JOIN
    item_category qc
ON
    q.category_id = qc.category_id;

