create database questions DEFAULT CHARACTER SET utf8;

use questions;

create table item_category(
	category_id int not null auto_increment primary key,
	category_name varchar(256) not null
);

