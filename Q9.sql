SELECT
	qc.category_name,
	SUM(q.item_price) AS total_price
FROM
	item q
INNER JOIN
	item_category qc
ON
	q.category_id = qc.category_id
GROUP BY
	q.category_id
ORDER BY
	total_price DESC;
